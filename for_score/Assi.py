"""
    @Author: zhoucx
    @CreateTime: 2021-06-29 21:58
    @Description: todo
"""

import sys, os

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))

import get_answers.Fetcher as fetcher
import get_answers.CollectAnswers as collect_ans

fetcher.readOpenID()

TABLE_NAME = 'titles'
import db.data_manager

dataManager = db.data_manager.DataManager()

import time, random


def calcAns(questions):
    answers = []
    for idx, q in enumerate(questions, 1):
        _id = q['id']
        anAns = dataManager.queryCol(TABLE_NAME, ['answer'], {'id': _id})
        if not anAns:
            anAns = '1'
        else:
            anAns = anAns[0][0]
        answers.append(anAns)
    
    return "}".join(['%s$%s' % (idx, ans) for (idx, ans) in enumerate(answers, 1)])


def onePaper():
    code = fetcher.f_create()
    paperId, sign, questions = fetcher.f_titles(code)
    answer = calcAns(questions)
    
    slpTime = random.randint(100, 200)
    print("%s秒后交卷" % (slpTime))
    time.sleep(slpTime)  # 拿到试卷后，等X秒交卷
    fetcher.f_commit(paperId, sign, answer)

    # 尝试更新题库
    answer = fetcher.f_page(paperId, sign)
    answer = fetcher.formatAnswer(answer)
    for anAns in answer:
        if collect_ans.addNewTitle(anAns, dataManager):
            print("题库扩充！！！")
        

def main():
    for i in range(0, 21):
        onePaper()
        slpTime = random.randint(60, 120)
        print("%s秒后开始下一套试卷" % (slpTime))
        time.sleep(slpTime)  # 刷完一道题后，等X秒刷下一套
        print("loop times: %s" % (i))


if __name__ == '__main__':
    main()
