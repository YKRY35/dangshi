### 使用说明

> 操作

1. 微信打开页面，登记注册完信息后，右上角三个点，“在默认浏览器中打开”。

2. 打开后把链接复制出来，记下openid=后面的哈希串，纯字母，32个字母。不包括结尾可能的# /。

3. 例如:  bba280681ab825bxxxxxx4091c69885f

4.  运行

   4.1 运行方式1

   > openid从本地文件读取
   >
   > 在for_score目录下，新建open_id.txt，填入这个哈希串，写在第一行（首位空格换行会被自动忽视）。
   >
   > 运行run_for_score.bat开始刷。

   

   4.2 运行方式2

   > openid 从命令行获取
   >
   > 在for_score目录下，命令行运行: python Assi.py --o bba280681ab825bxxxxxx4091c69885f 即可传入open_id
   >
   > 开始刷






> 运行环境

​	Python3 （开发基于Python 3.6.4)



> 依赖

​	requests       sqlite3     getopt



> 参数（刷题等待，一张卷时间，刷题套数）

​	见for_score/Assi.py 仅有的2条sleep注释。



### 模块介绍

> db

​	数据库相关，数据库使用内存数据库，数据存储在db/ds.db中，已维护好题库。



> get_answers

​	爬取答案，存入ds.db



> for_score

​	刷题。



### 免责声明

仅供学习，交流。。。





