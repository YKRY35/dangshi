"""
    @Author: zhoucx
    @CreateTime: 2021-06-29 17:20
    @Description: todo
"""

import get_answers.Fetcher as AF
import db.data_manager, json
import time

TABLE_NAME = 'titles'


def getExistsIds(dataManager):
    ids = dataManager.queryCol(TABLE_NAME, ['id'])
    return dict([(n[0], 1) for n in ids])

def addNewTitle(anAns, dataManager, existsIds = None):
    _id, _title, _type, _content, _answer = anAns
    # print(_id, _title, _type, cont, _content)
    
    if existsIds != None: # 从数据结构中获取重复信息
        if _id in existsIds: return False
        existsIds[_id] = 1
    else: # 从数据库中获取重复信息
        ids = getExistsIds(dataManager)
        if _id in ids: return False
        
    dataManager.insert(TABLE_NAME, {
        'id': _id,
        'title': _title,
        'type': _type,
        'content': json.dumps(_content),
        'answer': _answer
    })
    return True

def main():
    dataManager = db.data_manager.DataManager()
    
    existsIds = getExistsIds(dataManager)
    print(existsIds)
    
    try:
        for i in range(0, 300):
            ans = AF.main()
            for anAns in ans:
                addNewTitle(anAns, dataManager, existsIds)
                time.sleep(0.5)
            print("task: %s, exists num: %s"% (i, len(existsIds)))
    except KeyboardInterrupt:
        del dataManager
        print("exit due to keyboard interrupt")
        


if __name__ == '__main__':
    main()
