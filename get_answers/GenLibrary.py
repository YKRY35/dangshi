"""
    @Author: zhoucx
    @CreateTime: 2021-06-29 22:25
    @Description: todo
"""


TABLE_NAME = 'titles'
import db.data_manager
dataManager = db.data_manager.DataManager()

import json

def getFormattedData():
    res = []
    data = dataManager.queryCol('titles', ['title', 'type', 'content'])
    for line in data:
        ansTmp = json.loads(line[2])
        ans = [a['item'] for a in ansTmp if a['right']]
        res.append((line[0], line[1], ans))
    return res

def typeToName(_type):
    if _type == '1':
        return '单选题'
    if _type == '2':
        return '判断题'
    return '多选题'

def getHtmlContent(data):
    res = '<html><head><meta charset="UTF-8"><title>党史题库 %s题</title></head><body><div style="margin-left:50px; margin-top: 50px;">' % (len(data))
    for line in data:
        _title, _type, _ans = line[0], line[1], line[2]
        
        res += '<div style="background: #F2F7FA; padding: 0 2% 2%; margin-bottom: 32px;">'
        res += '<br>'
        res += '<p> 【%s】 %s </p>' % (typeToName(_type), _title)
        res += '<p>'
        res += ' <br> '.join(_ans)
        res += '</p>'
        
        res += '</div>'
    res += '</div></body></html>'
    return res

def main():
    data = getFormattedData()
    htmlContent = getHtmlContent(data)
    with open('output/lib.html', 'w', encoding='utf-8') as f:
        f.write(htmlContent)
    

if __name__ == '__main__':
    main()
    