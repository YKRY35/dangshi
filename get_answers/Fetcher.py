import requests
import time

OPEN_ID = ''

HEADERS = {
    # 'Host': 'Host: dsdt.people.cn',
    'Referer': 'http://dangshi.people.com.cn/',
    'Origin': 'http://dangshi.people.com.cn',
    'User-Agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_1_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/16D57 MicroMessenger/7.0.3(0x17000321) NetType/WIFI Language/zh_CN'
}


def readOpenID():
    global OPEN_ID

    # 从命令行获取
    import getopt, sys
    shortopts, longopts = 'o:', ['openid=']  # -o --openid=
    optlist, args = getopt.getopt(sys.argv[1:], shortopts, longopts)
    for key, val in optlist:
        if key in ('-o', '--openid'):
            if len(val) != 32:
                print("OPEN_ID 需要32位")
                exit(1)
            OPEN_ID = val
            print("OPEN_ID = %s . (read from command line)" % (OPEN_ID))
            break

    # 不存在，则从本地文件读取
    if not OPEN_ID:
        with open("open_id.txt") as f:
            OPEN_ID = f.readline().strip()
            print("OPEN_ID = %s . (read from open_id.txt)" % (OPEN_ID))


def getTimestamp():
    return int(time.time())


def f_create():
    resp = requests.post('https://dsdt.people.cn/dangshi_jr/public/index.php/jiangxi/paper/create', headers=HEADERS,
                         params={
                             'openid': OPEN_ID,
                             # 'time': getTimestamp()
                         })
    # print(resp.text)
    data = resp.json()
    if data['status'] != 1:
        print("f_create status != 1")
        exit(1)

    return data['data']['code']


def f_titles(code):
    resp = requests.get('https://dsdt.people.cn/dangshi_jr/public/index.php/api/paper/%s' % code, headers=HEADERS)
    data = resp.json()
    if data['status'] != 1:
        print("f_titles status != 1")
        exit(1)
    return data['data']['id'], data['data']['commit_sign'], data['data']['questions']


def f_commit(paperId, sign, content=None):
    resp = requests.post('https://dsdt.people.cn/dangshi_jr/public/index.php/jiangxi/commit', headers=HEADERS, data={
        'openid': OPEN_ID,
        'paper_id': paperId,
        'sign': sign,
        'content': '1$1}2$1}3$1}4$1}5$1}6$1}7$1}8$1}9$1}10$1}11$1}12$1}13$1}14$1}15$1}16$1}17$1}18$1}19$1}20$1' if not content else content
    })
    resp.text


def get_score_list():
    resp = requests.get("https://dsdt.people.cn/dangshi_jr/public/index.php/api/score_list", params={
        'openid': OPEN_ID,
        'sort_param': "created_at",
        'sort_type': "DESC",
        'page': 1,
        'limt': 10
    })

    return resp.json()


def f_page(paperId, sign):
    resp = requests.get('https://dsdt.people.cn/dangshi_jr/public/index.php/api/commit/paper', headers=HEADERS, params={
        'paper_id': paperId,
        'sign': sign
    })
    data = resp.json()
    if data['status'] != 1:
        print("f_page status != 1")
        exit(1)
    return data['data']['questions']


def formatAnswer(answer):
    res = []
    for anAns in answer:
        _id, _title, _type = anAns['id'], anAns['title'], anAns['type']
        _content = anAns['content']
        _choo = []
        for idx, _c in enumerate(_content, 1):
            _right = _c['right']
            if _right: _choo.append(str(idx))
        _answer = '|'.join(_choo)
        res.append((_id, _title, _type, _content, _answer))
    return res


def main():
    readOpenID()
    code = f_create()
    paperId, sign, _ = f_titles(code)
    f_commit(paperId, sign)
    answer = f_page(paperId, sign)
    # print(answer)
    res = formatAnswer(answer)
    return res

def test():
    readOpenID()
    print(get_score_list())

if __name__ == '__main__':
    # print(main())

    test()
