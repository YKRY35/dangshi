import requests
import sys

import get_answers.Fetcher as fetcher

HEADERS = fetcher.HEADERS

def getOpenId(rand_sign):
    resp =requests.get("http://dsdt.people.cn/dangshi_jr/public/index.php/api/get_openid?rand_sign=%s" % rand_sign, headers=HEADERS)

    data = resp.json()
    print(data)
    if data['status'] != 1:
        print("error getOpenId")
        exit(1)
    return data['data']['openid']


def main():
    print(getOpenId("8c6a62ff4d4aefebf39566e6b724dde8"))

if __name__ == '__main__':
    main()
