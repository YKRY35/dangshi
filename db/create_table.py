"""
    @Author: zhoucx
    @CreateTime: 2021-06-29 16:58
    @Description: todo
"""
import sqlite3

def create_table():
    SQL= '''
        CREATE TABLE `titles`  (
          `id` varchar(50) NOT NULL,
          `title` text,
          `type` varchar(50),
          `content` text,
          `answer` text
        );
    '''
    conn = sqlite3.connect('ds.db')
    c = conn.cursor()
    c.execute(SQL)
    conn.commit()
    conn.close()

create_table()