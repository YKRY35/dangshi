"""
    @Author: zhoucx
    @CreateTime: 2021-06-29 17:23
    @Description: todo
"""

import sqlite3, os
class DataManager():

    NULL_VAL = '!@#$%^&'
    NOT_NULL_VAL = '&^%$#@!'
    
    def __init__(self):
        self.connection = sqlite3.connect(os.path.join(os.path.dirname(__file__), 'ds.db'))

    def __del__(self):
        if(self.connection):
            self.connection.close()
            
    def formatData(self, data):
        if not data: return data
        res = {}
        for key, val in data.items():
            if isinstance(val, str):  # 字符串 ' -> \'
                res[key] = val.replace("'", "\\'")
            else:
                res[key] = val
        return res

    def generateKEV(self, data):
        def getVal(v):
            if isinstance(v, str):
                return "'{}'".format(v)
            return "{}".format(v)
    
        def getKeyValPair(k, v):
            if v == self.NULL_VAL:
                return "`{}` is NULL".format(k)
            elif v == self.NOT_NULL_VAL:
                return "`{}` is not NULL".format(k)
            return "`{}`={}".format(k, getVal(v))
    
        return [getKeyValPair(k, v) for (k, v) in data.items()]
        
    def query(self, table, cond=None):
        cond = self.formatData(cond)
    
        sql = "select * from `{}`".format(table)
        if (cond):
            afterWhere = " and ".join(self.generateKEV(cond))
            sql = "{} where {}".format(sql, afterWhere)
        cursor = self.connection.cursor()
        cursor.execute(sql)
        return cursor.fetchall()

    def queryCol(self, table, cols: list, cond=None):
        if (cond):
            cond = self.formatData(cond)
    
        afterSelect = ", ".join(["`{}`".format(n) for n in cols])
        sql = "select {} from `{}`".format(afterSelect, table)
        if (cond):
            afterWhere = " and ".join(self.generateKEV(cond))
            sql = "{} where {}".format(sql, afterWhere)
        cursor = self.connection.cursor()
        cursor.execute(sql)
        return cursor.fetchall()
    
    def insert(self, table, tar):
        tar = self.formatData(tar)
        _keys, _vals = [], []
        for k, v in tar.items():
            _keys.append('`%s`' % (k))
            _vals.append('\'%s\'' % (v))
            
        sql = "insert into `{}` ({}) values ({})".format(table, ",".join(_keys), ",".join(_vals))
        # with self.connection.cursor() as cursor:
        cursor = self.connection.cursor()
        try:
            cursor.execute(sql)
            self.connection.commit()
        except Exception:
            print("!!")
            pass

    def update(self, table, cond, data):
        data = self.formatData(data)

        afterSet = " , ".join(self.generateKEV(data))
        sql = "update `{}` set {}".format(table, afterSet)
        if cond:
            afterWhere = " and ".join(self.generateKEV(cond))
            sql = "{} where {}".format(sql, afterWhere)
        cursor = self.connection.cursor()
        try:
            cursor.execute(sql)
            self.connection.commit()
        except Exception:
            pass
        